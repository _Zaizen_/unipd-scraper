import requests
import json

'''Link of the website to scrape
'''
url = "https://agendastudentiunipd.easystaff.it/call_redis.php?key=UNIPD_2021_ec_docenti_combo"
url2 = "https://agendastudentiunipd.easystaff.it/grid_call.php?form-type=docente&include=docente&anno=2021&docente=%s"

# Creation of headers for the requests
headers = requests.utils.default_headers()
headers.update({
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
     'Cookie': 'AWSALB=nMwgJQG2kSrT9i8zLCSV377Re3tIjZ6X7BtQQ25ccs5ozYu4XLhghQfa0VAbvsj3bu+DjjgwywMMk0BXzuole9pF7FlK+waKZUgwBW/RguC+tACY4SvBw+LHQu6q'
})
# Request to get the website pages using the headers
insegnantiJSON = requests.get(url, headers=headers)
insegnantiJSON = insegnantiJSON.text[21:-1]
insegnantiJSON = json.loads(insegnantiJSON)
elencoJSON = insegnantiJSON[0]["elenco"]

insegnamenti = {}

'''
ESEMPIO
insegnamenti = {
    "DIRITTO CIVILE": ["Cognome1", "Cognome2"]
}
'''

for i in range(len(elencoJSON)):
    cognome = elencoJSON[i]["label"].split(" ", 1)[0]
    print(cognome)
    id = elencoJSON[i]["valore"]
    buffer = url2 % id
    materieJSON = requests.post(buffer, headers=headers)
    materieJSON = materieJSON.json()
    materieJSON = materieJSON["legenda"]
    for i in range(len(materieJSON)):
        nomeMateria = materieJSON[i]["nome"]
        print(nomeMateria)
        if nomeMateria in insegnamenti:
            insegnamenti[nomeMateria].append(cognome)
        else:
            insegnamenti[nomeMateria] = [cognome]

with open('insegnamenti.json', 'w') as outfile:
    json.dump(insegnamenti, outfile, ensure_ascii=False, sort_keys=True, indent=4)